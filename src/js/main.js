$(document).ready(function () {
  'use strict';

  // page preloader
  $(window).on('load', function () {
    $('#page-preloader').fadeOut(function () {
      $(this).remove();
    });
    $('body').removeClass('no-scroll');
  });
  setTimeout(function () {
    if ($("#page-preloader").length) {
      $("#page-preloader").fadeOut(function () {
        $(this).remove();
      });
      $('body').removeClass('no-scroll');
    }
  }, 2000);

  // imgLiquid init
  $(".img-fill").imgLiquid({
    fill: true,
    horizontalAlign: "center",
    verticalAlign: "center"
  });
  $(".img-no-fill").imgLiquid({
    fill: false,
    horizontalAlign: "center",
    verticalAlign: "center"
  });

  // dotdotdot
  $('.dot-text').dotdotdot({watch: true});
  $('.dot-long').dotdotdot({watch: true});

  // input tel mask init
  $("input[type='tel']").mask("?+7 (999) 999-9999");

  // ==============================================================
  // pages global logic
  $('#header-search input').keyup(function () {
    if (($(this).val()) != "") {
      $('.lupa-search').show();
    } else {
      $('.lupa-search').hide();
    }
  });
  $('.btn-sidebar').click(function () {
    $('.sidebar').toggleClass('active-sidebar');
  });
  var windowWidth = window.innerWidth
      || document.documentElement.clientWidth
      || document.body.clientWidth;
  if (windowWidth > 1200)
    $(".sidebar").stick_in_parent();
  // ==============================================================
  // pages logic
  if ($("#feedBackModal").length) {
    $('#feedBackModalBtn').on('click', function () {
      $('#feedBackModal form').validate({
        highlight: function (el, errorClass, validClass) {
          $(el).parents('.form-group').addClass('has-error').removeClass('has-success');
        },
        unhighlight: function (el, errorClass, validClass) {
          $(el).parents('.form-group').addClass('has-success').removeClass('has-error');
        }
      });
    });
  }
  if ($(".page-index").length) {
    var swiper1 = new Swiper('.slider-block .slider-wrap .swiper-container', {
      on: {
        init: function () {
          console.log('swiper initialized');
        }
      },
      loop: true,
      spaceBetween: 40,
      autoplay: {
        delay: 3000,
      },
      pagination: {
        el: '.swiper-pagination',
        dynamicBullets: false,
      },
      /*navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
      },*/
    });
    var swiper2 = new Swiper('.slider-block .discont-wrap .swiper-container', {
      on: {
        init: function () {
          console.log('swiper initialized');
        }
      },
      loop: true,
      spaceBetween: 30,
      autoplay: {
        delay: 3000,
      },
    });
    var swiper3 = new Swiper('.product-block .swiper-container', {
      on: {
        init: function () {
          console.log('swiper initialized');
        }
      },
      //loop: true,
      slidesPerView: 4,
      spaceBetween: 30,
      breakpoints: {
        480: {
          slidesPerView: 1,
          spaceBetween: 10
        },
        768: {
          slidesPerView: 2,
          spaceBetween: 20
        },
        992: {
          slidesPerView: 3,
          spaceBetween: 30
        }
      }
    });
  }
  if ($(".page-news_detail").length) {
    var swiper1 = new Swiper('.other-block .swiper-container', {
      on: {
        init: function () {
          console.log('swiper initialized');
        }
      },
      //loop: true,
      slidesPerView: 4,
      spaceBetween: 30,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
      },
      breakpoints: {
        480: {
          slidesPerView: 1,
          spaceBetween: 10
        },
        768: {
          slidesPerView: 2,
          spaceBetween: 20
        },
        992: {
          slidesPerView: 3,
          spaceBetween: 30
        }
      }
    });
  }
  if ($(".page-product_list").length) {
    $('.hide-btn').click(function () {
      if ($('.filters-wrap').is(":visible")) {
        $('.filters-wrap').hide(300);
        $('.submit-btn').toggle();
        $('.hide-btn').text("Показать фильтры");
      }
      else if ($('.filters-wrap').is(":hidden")) {
        $('.filters-wrap').show(300);
        $('.hide-btn').text("Скрыть фильтры");
        $('.submit-btn').toggle();
      }
    });
  }
  if ($(".page-product_card").length) {
    if ($(".gallery-top").length && $(".gallery-thumbs").length) {
      var galleryTop = new Swiper('.slider-wrap .gallery-top', {
        spaceBetween: 10
      });
      var galleryThumbs = new Swiper('.slider-wrap .gallery-thumbs', {
        spaceBetween: 10,
        slidesPerView: 4,
        breakpoints: {
          480: {
            slidesPerView: 1,
            spaceBetween: 10
          },
          768: {
            slidesPerView: 2,
            spaceBetween: 20
          },
          992: {
            slidesPerView: 3,
            spaceBetween: 30
          }
        },
        centeredSlides: true,
        touchRatio: 0.4,
        slideToClickedSlide: true
      });
      galleryTop.controller.control = galleryThumbs;
      galleryThumbs.controller.control = galleryTop;
    }

    var swiper1 = new Swiper('.other-block .swiper-container', {
      on: {
        init: function () {
          console.log('swiper initialized');
        }
      },
      //loop: true,
      slidesPerView: 4,
      spaceBetween: 10,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
      },
      breakpoints: {
        480: {
          slidesPerView: 1,
          spaceBetween: 10
        },
        768: {
          slidesPerView: 2,
          spaceBetween: 20
        },
        992: {
          slidesPerView: 3,
          spaceBetween: 30
        }
      }
    });

    $('.selectpicker').selectpicker();
  }
  if ($(".page-cart").length) {
    $(".item-counter").TouchSpin({
      min: 0,
      max: 9999,
      step: 1,
      maxboostedstep: 10,
    });
    $('.selectpicker').selectpicker();
    $('#orderModalBtn').on('click', function () {
      $('#orderModal form').validate({
        highlight: function (el, errorClass, validClass) {
          $(el).parents('.form-group').addClass('has-error').removeClass('has-success');
        },
        unhighlight: function (el, errorClass, validClass) {
          $(el).parents('.form-group').addClass('has-success').removeClass('has-error');
        }
      });
    });
  }

});

